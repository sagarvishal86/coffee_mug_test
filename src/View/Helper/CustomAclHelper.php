<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

use Acl\Controller\Component\AclComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;





class CustomAclHelper extends Helper
{
    // initialize() hook is available since 3.2. For prior versions you can
    // override the constructor if required.
    public function initialize(array $config)
    {
     //   debug($config);
    }

    public function checkPermission($aro,$aco,$action){
        $collection = new ComponentRegistry();
        $this->Acl = new AclComponent($collection, Configure::read('Acl'));

        if($this->Acl->check(array('model' => $aro, 'foreign_key' => $aco), $action)){
            return true;
        } else {
            return false;
        }
    }
}