-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 09, 2021 at 01:10 PM
-- Server version: 5.7.33-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-50+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake_acl`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_phinxlog`
--
CREATE DATABASE cake_acl;

USE cake_acl;

CREATE TABLE `acl_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_phinxlog`
--

INSERT INTO `acl_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20141229162641, 'CakePhpDbAcl', '2021-05-08 06:35:17', '2021-05-08 06:35:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 106),
(2, 1, NULL, NULL, 'Posts', 2, 13),
(3, 2, NULL, NULL, 'index', 3, 4),
(4, 2, NULL, NULL, 'view', 5, 6),
(5, 2, NULL, NULL, 'add', 7, 8),
(6, 2, NULL, NULL, 'edit', 9, 10),
(7, 2, NULL, NULL, 'delete', 11, 12),
(8, 1, NULL, NULL, 'Groups', 14, 25),
(9, 8, NULL, NULL, 'index', 15, 16),
(10, 8, NULL, NULL, 'view', 17, 18),
(11, 8, NULL, NULL, 'add', 19, 20),
(12, 8, NULL, NULL, 'edit', 21, 22),
(13, 8, NULL, NULL, 'delete', 23, 24),
(14, 1, NULL, NULL, 'Users', 26, 43),
(15, 14, NULL, NULL, 'index', 27, 28),
(16, 14, NULL, NULL, 'view', 29, 30),
(17, 14, NULL, NULL, 'add', 31, 32),
(18, 14, NULL, NULL, 'edit', 33, 34),
(19, 14, NULL, NULL, 'delete', 35, 36),
(20, 14, NULL, NULL, 'login', 37, 38),
(21, 14, NULL, NULL, 'logout', 39, 40),
(22, 1, NULL, NULL, 'Widgets', 44, 55),
(23, 22, NULL, NULL, 'index', 45, 46),
(24, 22, NULL, NULL, 'view', 47, 48),
(25, 22, NULL, NULL, 'add', 49, 50),
(26, 22, NULL, NULL, 'edit', 51, 52),
(27, 22, NULL, NULL, 'delete', 53, 54),
(28, 1, NULL, NULL, 'Pages', 56, 59),
(29, 28, NULL, NULL, 'display', 57, 58),
(30, 1, NULL, NULL, 'Error', 60, 61),
(31, 1, NULL, NULL, 'Acl', 62, 63),
(32, 1, NULL, NULL, 'Bake', 64, 65),
(33, 1, NULL, NULL, 'DebugKit', 66, 101),
(34, 33, NULL, NULL, 'DebugKit', 67, 68),
(35, 33, NULL, NULL, 'MailPreview', 69, 76),
(36, 35, NULL, NULL, 'index', 70, 71),
(37, 35, NULL, NULL, 'sent', 72, 73),
(38, 35, NULL, NULL, 'email', 74, 75),
(39, 33, NULL, NULL, 'Panels', 77, 82),
(40, 39, NULL, NULL, 'index', 78, 79),
(41, 39, NULL, NULL, 'view', 80, 81),
(42, 33, NULL, NULL, 'Dashboard', 83, 88),
(43, 42, NULL, NULL, 'index', 84, 85),
(44, 42, NULL, NULL, 'reset', 86, 87),
(45, 33, NULL, NULL, 'Composer', 89, 92),
(46, 45, NULL, NULL, 'checkDependencies', 90, 91),
(47, 33, NULL, NULL, 'Requests', 93, 96),
(48, 47, NULL, NULL, 'view', 94, 95),
(49, 33, NULL, NULL, 'Toolbar', 97, 100),
(50, 49, NULL, NULL, 'clearCache', 98, 99),
(51, 1, NULL, NULL, 'Migrations', 102, 103),
(52, 1, NULL, NULL, 'WyriHaximus\\TwigView', 104, 105),
(53, 14, NULL, NULL, 'view', 41, 42);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Groups', 1, NULL, 1, 4),
(2, NULL, 'Groups', 2, NULL, 5, 8),
(3, 1, 'Users', 1, NULL, 2, 3),
(4, 2, 'Users', 2, NULL, 6, 7);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '1', '1', '1', '1'),
(2, 1, 2, '1', '1', '1', '1'),
(3, 1, 22, '1', '1', '1', '1'),
(4, 2, 1, '-1', '-1', '-1', '-1'),
(5, 2, 3, '1', '1', '1', '1'),
(6, 2, 4, '1', '1', '1', '1'),
(7, 2, 21, '1', '1', '1', '1'),
(8, 2, 10, '1', '1', '1', '1'),
(9, 2, 9, '1', '1', '1', '1'),
(10, 2, 53, '1', '1', '1', '1'),
(11, 2, 15, '1', '1', '1', '1'),
(12, 2, 5, '1', '1', '1', '1'),
(13, 2, 6, '1', '1', '1', '1'),
(14, 2, 7, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Admin', '2021-05-08 12:09:52', '2021-05-08 12:09:52'),
(2, 'User', '2021-05-08 12:10:03', '2021-05-08 12:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `created`, `modified`) VALUES
(1, 2, 'test post', 'this is test post by end user', '2021-05-08 12:15:18', '2021-05-08 12:15:18'),
(2, 2, 'second post', '2nd post by end user', '2021-05-08 12:17:25', '2021-05-08 12:17:25'),
(3, 1, 'admin test post', 'admin test post', '2021-05-08 12:19:43', '2021-05-08 12:19:43'),
(4, 2, '4th post', 'this is my fouth post', '2021-05-08 12:27:55', '2021-05-08 12:27:55'),
(5, 2, '5th', 'this is my 5 post', '2021-05-08 12:28:58', '2021-05-08 12:28:58'),
(6, 2, '6', 'this is my 6 post', '2021-05-08 12:29:12', '2021-05-08 12:29:12'),
(7, 2, '7', 'this is my 7 post', '2021-05-08 12:29:23', '2021-05-08 12:29:23'),
(8, 2, '8', 'this is my 8th post', '2021-05-08 12:29:38', '2021-05-08 12:29:38'),
(9, 2, '9', 'this is my 9 post', '2021-05-08 12:29:52', '2021-05-08 12:29:52'),
(10, 2, '10', 'this is my 10 post', '2021-05-08 12:30:04', '2021-05-08 12:30:04'),
(11, 2, '11', 'this is my 11 post', '2021-05-08 12:30:14', '2021-05-08 12:30:14'),
(12, 2, 'coffee', 'coffee mug coffee mug coffee mug ', '2021-05-09 02:59:50', '2021-05-09 02:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `group_id`, `created`, `modified`) VALUES
(1, 'vishal88', '$2y$10$ilQ36zreLqxEPhTpwfLUnu87mHRex/X1iGmrn2Zchzd/Y4hXDkp5u', 1, '2021-05-08 12:10:13', '2021-05-08 12:10:13'),
(2, 'super30', '$2y$10$3h9rq9R7GtxSnZK9lsm/4OD4cJHGyEVXJiTgrd3GHvgqB/4mc01q2', 2, '2021-05-08 12:10:26', '2021-05-08 12:10:26');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `part_no` varchar(12) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_phinxlog`
--
ALTER TABLE `acl_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aro_id` (`aro_id`,`aco_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
